<?php header('Content-Type: image/svg+xml');

require 'callis.php';

$dir = new Vec2(128, 0);
$callis = new Callis();
$callis->width[0] = 0;

$n = 32;
for ($i = 0; $i < $n; ++$i)
{

    $in = 0 + 2 * ($i ==0);// * ($i % 3 == 0);//$i % 3;
//    if ($i < 2)
//        $in = $i;
//    elseif ($i > 4)
//        $in = 6 - $i;

    $out = -3 / (2 * $i / sqrt($n) + 1) * ($i != $n - 1);// * ((1 + $i) % 3 == 0);//-(($i + 1) % 3);
//    if ($i > 2)
//        $out = min($i - 2, 2);
//    if ($i > 8)
//        $out = -2 + ($i % 2) * 4;
//    if ($i > 24)
//        $out = 22 - $i;

//    $curve->offset = new Vec2(2.5 * $dir->x * ($i % 9), 1.75 * $dir->x * floor($i / 9));
    $callis->addSegment(new Witch(vecAdd(new Vec2(sin(3 * $i/sqrt($n)) * 512, cos(3*$i/sqrt($n)) * 512), $dir), $in, $out), (32 + pow(0.023 * $n * ($n - $i) + 2, 2)) * ($i != $n - 1));

//    $brew->merge($curve);
}
echo $callis->wrapped()."\n";
?>
