<?php header('Content-Type: image/svg+xml');

function randomColor() {
    $col = "#";
    $h = rand() % 3;
    for ($i = 0; $i < 6; ++$i)
    {
        if ($i % 2 == 0 && $h == floor($i / 2))
            $sig = 8;
        else
            $sig = 0;
        $col .= dechex($sig + rand() % (16 - $sig));
    }
    return $col;
}

require 'witch.php';

$n = 32;
$x = -256;
$c = new Curve("", "black"/*randomColor()*/);

for ($i = 0; $i < $n; ++$i)
{
    $size = 32 + rand() % 16;
    $d = new Vec2(min(128 - $x, (rand() % 320) - 160 - $x / 32), -$size);
    if ($i == $n - 1)
        $d->x = -$x * 0.25;

    if (count($c->segments)) {
        $endSeg = $c->segments[count($c->segments) - 1];
        $th = $endSeg->delta->angleTo($d);
        $in = -$endSeg->sOut() - $th;
    }
    else
        $in = $d->angleTo(new Vec2(-1, 0));

    $out = (2 + ($n - $i) / $n) * (rand() % 5 == 0) * (1 - rand() % 3) * (0.5 + (rand() % 9) / 4);

    $seg = new Witch($d, $in, $out);

    $c->addSegment($seg);

    $x += $d->x;
}
for ($i = $n - 1; $i >= 0; --$i)
{
    $seg = $c->segments[$i];
    $w2 = new Witch(vecMul($seg->delta, new Vec2(1, -1)), $seg->sOut(), $seg->sIn());
    $c->addSegment($w2);
}

$compose = new SVGOBject();
$compose->merge($c);

$trace = new Curve("#fff6"/*randomColor()*/);   
        $trace->strokeWidth = 1.5;
for ($i = 0; $i < count($c->segments); ++$i)
{
    $seg = $c->segments[$i];
//    if ($i % 2) {


        $w = new Segment($seg->delta, $seg->sIn(), $seg->sOut());
//        $trace->addSegment($w);
//    }
}
$compose->merge($trace);

echo $compose->wrapped()."\n";
?>
