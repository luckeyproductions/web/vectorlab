<?php
require 'svgobject.php';

class Segment {

    public $delta;
    public $spokes;

    public function __construct($delta, $in = 0, $out = 0) {

        $this->delta  = $delta;
        $this->spokes = new Vec2($in, $out);
    }

    public function sIn()  { return $this->spokes->x; }
    public function sOut() { return $this->spokes->y; }
    public function nIn()  { return vecRot(vecNorm($this->delta), -2 - $this->spokes->x); }
    public function nOut() { return vecRot(vecNorm($this->delta), -2 + $this->spokes->y); }
    public function cIn()  { return vecScale(vecRot($this->nIn(), 2), $this->delta->mag() * 0.34); }
    public function cOut() { return vecAdd($this->delta, vecScale(vecRot($this->nOut(), -2), $this->delta->mag() * 0.34)); }

    public function toStr() {
        if ($this->sIn() == 0 && $this->sOut() == 0)
            return $this->delta->x.",".$this->delta->y;
        else {
            $c1 = $this->cIn();
            $c2 = $this->cOut();
            return $c1->x.",".$c1->y." ".$c2->x.",".$c2->y." ".$this->delta->x.",".$this->delta->y;
        }
    }
}

class Curve extends SVGObject {

    public $strokeWidth;
    public $stroke;
    public $fill;
    public $segments;
    public $offset;

    public function __construct($stroke = "red", $fill = "none") {
        $this->strokeWidth = 1;
        $this->stroke = strlen($stroke) ? $stroke : "none";
        $this->fill = strlen($fill) ? $fill : "none";
        $this->segments = array();
        $this->offset = new Vec2();
        parent::__construct(0, 0, 0, 0);
    }

    public function addSegment($seg) {
        $endPoint = $this->endPoint();
        array_push($this->segments, $seg);
        
        $newPoint = vecAdd($endPoint, $seg->delta);
        $mid = vecLerp($endPoint, $newPoint);

        for ($i = 0; $i < 4; ++$i)
        {
            $shift = new Vec2($seg->delta->mag() / 2 + min(0.8, sqrt(0.023 * max(0, max(abs($seg->sIn()), abs($seg->sOut())) - 2))) * $seg->delta->mag(), 0);
            $shift = vecRot($shift, 2 * $i);
            $this->fixBounds(vecAdd($mid, $shift));
        }
    }

    public function endPoint() {
        $point = $this->offset;
        foreach ($this->segments as $seg) {
            $point = vecAdd($point, $seg->delta);
        }
        return $point;
    }

    public function body() {

        $mode = "";

        $body = parent::body();
        if (strlen($body))
            $body.= "\n";

        $body .= "<path \n".
                "fill='".$this->fill."'\n".
                "stroke='".$this->stroke."'\n".
                "stroke-width='".$this->strokeWidth."px'\n".
                "stroke-linecap='round'\n".
                "d='m ".$this->offset->x.",".$this->offset->y;

        foreach ($this->segments as $seg) {
            $lastMode = $mode;

            if ($seg->sIn() == 0 && $seg->sOut() == 0)
                $mode = "l";
            else
                $mode = "c";

            if ($mode != $lastMode)
                $body .= " ".$mode;

            $body .= " ".$seg->toStr();
        }
        $body .= "'\n/>";
        return $body;
    }
}
?>
