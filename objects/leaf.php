<?php header('Content-Type: image/svg+xml');

require 'witch.php';
$leaves = new SVGObject(-800, -400, 1600, 800);

for ($i = 0; $i < 420; ++$i)
{
    $c = new Curve("#3e7318a0", "#8".dechex(rand() % 16)."a".dechex(rand() % 16)."2b");

    $size = 8 + rand() % 16;

    $d = new Vec2($size * 8, 0);
    $d = vecRot($d, (rand() % 64) / 8);

    $in = 1 + (rand() % 9) / 8;
    $out = (rand() % 9) / 8;

    $w1 = new Witch($d, $in, $out);
    $w2 = new Witch(vecScale($d, -1), $out, $in);
    $c->addSegment($w1);
    $c->addSegment($w2);

    $c->offset = new Vec2(800 - rand() % 1600, 512 - rand() % 1024);
    $leaves->merge($c);
}
echo $leaves->wrapped()."\n";
?>
