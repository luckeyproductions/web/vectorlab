<?php
require 'curve.php';

class Witch extends Segment {

    public function toStr() {

        if ($this->sIn() == 0 && $this->sOut() == 0)
            return $this->delta->x.",".$this->delta->y;
        else {

            $witch = "";
            $offset = new Vec2(0, 0);
            $split = 2 + 2 * round(pow(abs($this->sIn()) + abs($this->sOut()), 1/3.));
            $th = 0;

            for ($i = 0; $i < $split; ++$i) {
                $t = $i / $split;

                $th -= 14/.9*step*(cIn + t * (cOut - cIn));
                $offset = vecAdd($offset, new Vec2(sin($th * pi() / (140/9.)), cos($th * pi() / (140/9.))));
            }

            return $witch;
        }
    }
}
?>
