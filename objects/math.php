<?php
function lerp($a, $b, $t = 0.5) {
    return $a + $t * ($b - $a);
}

function spoke2rad($s) {
    return $s * pi() / 4;
}
function rad2spoke($r) {
    return $r / (pi() / 4);
}

class Vec2 {
    public $x;
    public $y;

    public function __construct($x = 0, $y = 0) {
        $this->x = $x;
        $this->y = $y;
    }

    public function mag() {
        if ($this->x == 0 && $this->y == 0)
            return 0;
        else
            return sqrt($this->x * $this->x + $this->y * $this->y);
    }

    public function dot($v) { return $this->x * $v->x + $this->y * $v->y; }
    public function angleTo($v) {
        $v = vecNorm($v);
        $n = vecNorm($this);

        if ($n->mag() == 0 || $v->mag() == 0)
            return 0;
        else
        {
            $clockwise = rad2spoke(acos(vecRot($n, -2)->dot($v))) < 2 ? true : false;
            $angle = rad2spoke(acos($n->dot($v)));

            if (!$clockwise)
                $angle = -$angle;

            return $angle;
        }
    }
}

function vecNorm($v) {
    $m = $v->mag();

    if ($m == 0)
        return $v;
    else
        return new Vec2($v->x / $m, $v->y / $m);
}

function vecRot($v, $s) {
    if ($v->mag() == 0)
        return new Vec2();
    elseif ($s == 0)
        return $v;
    else
    {
        $th = spoke2rad($s);
        $cs = cos($th);
        $sn = sin($th);
        $px = $v->x * $cs - $v->y * $sn; 
        $py = $v->x * $sn + $v->y * $cs;

        return new Vec2($px, $py);
    }
}
function vecAdd($a, $b) { return new Vec2($a->x + $b->x, $a->y + $b->y); }
function vecSub($a, $b) { return new Vec2($a->x - $b->x, $a->y - $b->y); }
function vecMul($a, $b) { return new Vec2($a->x * $b->x, $a->y * $b->y); }
function vecScale($v, $s) { return new Vec2($v->x * $s, $v->y * $s); }

function vecLerp($a, $b, $t = 0.5) {
    $r = vecSub($b, $a);
    $r = vecScale($r, $t);
    $r = vecAdd($a, $r);

    return $r;
}
?>
