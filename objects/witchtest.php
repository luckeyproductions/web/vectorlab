<?php header('Content-Type: image/svg+xml');

require 'witch.php';

$dir = new Vec2(128, 0);
$brew = new SVGObject();

for ($i = 0; $i < 9*9; ++$i)
{
    $curve = new Curve("#".dechex($i * 1234 + 4281375));

    $in = 2;
    if ($i < 2)
        $in = $i;
    elseif ($i > 4)
        $in = 6 - $i;

    $out = 0;
    if ($i > 2)
        $out = min($i - 2, 2);
    if ($i > 8)
        $out = -2 + ($i % 2) * 4;
    if ($i > 24)
        $out = 22 - $i;

    $curve->offset = new Vec2(2.5 * $dir->x * ($i % 9), 1.75 * $dir->x * floor($i / 9));
    $curve->addSegment(new Witch($dir, $in, $out));

    $brew->merge($curve);
}
echo $brew->wrapped()."\n";
?>
