<?php
require 'curve.php';

class Witch extends Segment {

    public function toStr() {

        if ($this->sIn() == 0 && $this->sOut() == 0)
            return $this->delta->x.",".$this->delta->y;
        else {
            $witch = "";
            $offset = new Vec2(0, 0);
            $split = abs($this->sIn()) > 2 || abs($this->sOut()) > 2 ? true : false;
            $dt = $split ? 0.5 : 0.2;

            for ($t = 0; $t < 1.0; $t += $dt) {

                $first = $t == 0;
                $last = ($t + $dt * 2) > 1.0;

                if (!$first)
                    $witch .= " ";

                if (!$last)
                    $d = vecSub($this->plot($t + $dt), $offset);
                else
                    $d = vecSub($this->delta, $offset);

                $th = $d->angleTo($this->delta);

                if ($first)
                    $in = $this->sIn() + $th;
                else
                {
                    $n = $this->normal($t);
                    $in = -$n->angleTo(vecRot($d, -2));
                }

                if ($last)
                    $out = $this->sOut() - $th;
                else
                {
                    $n = $this->normal($t + $dt);
                    $out = $n->angleTo(vecRot($d, -2));
                }

                if ($split)
                {
                    $clampIn = min(4, max(-4, $this->sIn()));
                    $clampOut = min(4, max(-4, $this->sOut()));
                    $opposite = $clampIn >= 0 != $clampOut >= 0;
                    $splitSlope = ($clampOut - $clampIn) / 4;
                    if ($opposite)
                    {
                        $splitSlope *= -($first ? -$th : $th) * 1/5;
                    }
//                    $n = vecScale(vecNorm(vecRot($this->delta, -2)), (1 - $splitSlope * $splitSlope) * $this->delta->mag() / (2 + 1 * cos(spoke2rad($splitSlope / 2))));

                    if ($first)
                    {
                        if ($opposite)
                        {
//                            $d = VecAdd($d, $n);
                        }
                        $out -= $splitSlope;
                    }
                    else
                    {
                        if ($opposite)
                        {
//                            $d = VecAdd($d, -$n);
                        }
                        $in += $splitSlope;
                    }

                    $seg = new Witch($d, $in, $out);
                }
                else
                    $seg = new Segment($d, $in, $out);

                $witch .= $seg->toStr();
                $offset = vecAdd($offset, $d);
            }

            return $witch;
        }
    }

    public function plot($t) {

        $sinPiT = sin(pi() * $t);
        $cosPiT = cos(pi() * $t);
        $a = min(1.125, max(-1.125, lerp($this->sIn(), $this->sOut(), $t) / 2));
        $b = 1 + 0.22876384 * ($a * $a - 1) * pow($sinPiT, 0.475) * min(1, max(0, 0.25 * abs(4 - ($this->sIn() - $this->sOut()))));
        $x = lerp($t, (1 - $cosPiT) / 2, $a * $a);
        $y = lerp(0, -$sinPiT / 2, $a * $b);
        $m = $this->delta->mag();

        $point = new Vec2($x * $m, $y * $m);
        $point = vecRot($point, $this->delta->angleTo(new Vec2(1, 0)));

        return $point;
    }

    public function normal($t, $dt = 0.1) {
        $d = vecSub($this->plot($t + $dt), $this->plot($t - $dt));
        $d = vecNorm($d);
        $d = vecRot($d, -2);
        return $d;
    }
}
?>
