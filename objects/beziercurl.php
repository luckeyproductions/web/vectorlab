<?php header('Content-Type: image/svg+xml');

require 'curve.php';

$c = new Curve();

for ($i = 0; $i < 235; ++$i)
{
    $d = new Vec2();
    while ($d->mag() < 5)
        $d = new Vec2(-100 + rand() % 200, -100 + rand() % 200);

    if (count($c->segments)) {
        $endSeg = $c->segments[count($c->segments) - 1];
        $th = $endSeg->delta->angleTo($d);
        $sIn = -$endSeg->sOut() - $th;
    } else
        $sIn = -2 + rand() % 5;

    $seg = new Segment($d, $sIn, -2 + rand() % 5 );
    $c->addSegment($seg);
}

echo $c->wrapped()."\n";
?>
