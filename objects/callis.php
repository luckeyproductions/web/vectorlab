<?php
require 'witch.php';

class Callis extends Curve {

    public $width;
    public $cap;

    public function __construct($stroke = "none", $fill = "black") {
        $this->cap = false;
        $this->width = array(0);
        parent::__construct($stroke, $fill);
    }

    public function addSegment($seg, $width = 0) {

        if (is_null($width))
            $width = $this->width[count($this->width) - 1];

        array_push($this->width, $width);
        parent::addSegment($seg);
    }

    public function body() {
        $mode = "";
        $res = new Curve($this->stroke, $this->fill);
        $res->strokeWidth = $this->strokeWidth;
        $res->offset = $this->offset;

        $body = SVGObject::body();
        if (strlen($body))
            $body.= "\n";

        $body .= "<path \n".
                "fill='".$this->fill."'\n".
                "stroke='".$this->stroke."'\n".
                "stroke-width='".$this->strokeWidth."px'\n".
                "stroke-linecap='round'\n".
                "d='m ".$this->offset->x.",".$this->offset->y;

        for ($i = 0; $i < count($this->segments) * 2; ++$i) {

            $iMod = $i % count($this->segments);
            $j = $i; //segment to pick
            $rev = $j >= count($this->segments);
            if ($rev)
                $j = count($this->segments) - $iMod - 1;

            $lastMode = $mode;

            $seg = $this->segments[$j];
            $d = $seg->delta;
            if ($d->mag() == 0)
                continue;

            $nd = vecNorm(vecRot($d, -2));

            if ($j == 0)
                $n0 = $nd;
            else
            {
                $lnd = vecNorm(vecRot($this->segments[$j - 1]->delta, -2));
                $n0 = vecScale(vecAdd($nd, $lnd), 0.5);
            }

            if ($j == count($this->segments) - 1)
                $n1 = $nd;
            else
            {
                $nnd = vecNorm(vecRot($this->segments[$j + 1]->delta, -2));
                $n1 = vecScale(vecAdd($nd, $nnd), 0.5);
            }

            $w0 = $this->width[$j];
            $w1 = $this->width[$j + 1];
            $in = $seg->sIn();
            $out = $seg->sOut();

            if ($i == 0)
            {
                if (!$this->cap && $w0 != 0)
                {
                    $body .= move(vecScale($n0, $w0));
                    $mode = $lastMode = "m";
                }
            }

            if (!$rev)
            {
                $d = vecAdd($d, vecSub(vecScale($n1, $w1), vecScale($n0, $w0)));
                $nd = vecNorm(vecRot($d, -2));

                $in -= $n0->angleTo($nd);
                $out += $n1->angleTo($nd);

                $witch = new Witch($d, $in, $out);
            }
            else
            {
                $d = vecAdd($d, vecSub(vecScale($n0, $w0), vecScale($n1, $w1)));
                $nd = vecNorm(vecRot($d, -2));

                $in += $n0->angleTo($nd);
                $out -= $n1->angleTo($nd);

                $witch = new Witch(vecScale($d, -1), $out, $in);
            }

            if ($witch->sIn() == 0 && $witch->sOut() == 0)
                $mode = "l";
            else
                $mode = "c";

            if ($mode != $lastMode)
            {
                $body .= " ".$mode;
                $lastMode = $mode;
            }
            $body .= " ".$witch->toStr();

            if ($iMod == count($this->segments) - 1)
            {
                if (($rev && $w0 != 0) || (!$rev && $w1 != 0))
                {
                    if (!$this->cap)
                    {
                        if (!$rev)
                            $ld = vecScale($n1, -$w1 * 2);
                        else
                            $ld = vecScale($n0, $w0 * 2);

                        $body .= line($ld);
                        $mode = "l";
                    }
                }
            }
        }
        $body .= " z'\n/>";
        return $body;
    }
}

if (count($_GET))
{
    header('Content-Type: image/svg+xml');

    $delta = new Vec2(128, 0);
    $route = NULL;
    $n = 1;

    $startWidth = 0;
    $endWidth = 0;
    $widthPattern = array();
    $startSlope = 2;
    $endSlope = 0;
    $slopePattern = array();

    if (array_key_exists("w", $_GET))
    {
        $widthString = $_GET["w"];
        $widthArray = parse($widthString);
        if (!is_null($widthArray))
        {
            if (array_key_exists("start", $widthArray) && strlen($widthArray["start"][0]))
            {
                $startWidth = processValue($widthArray["start"][0]);
            }
            if (array_key_exists("end", $widthArray) && strlen($widthArray["end"][0]))
            {
                $endWidth = processValue($widthArray["end"][0]);
            }
            if (array_key_exists("pattern", $widthArray) && strlen($widthArray["pattern"][0]))
            {
                $widthPattern = $widthArray["pattern"];
            }
        }
    }
    if (array_key_exists("s", $_GET))
    {
        $slopeString = $_GET["s"];
        $slopeArray = parse($slopeString);

        if (!is_null($slopeArray))
        {
            if (array_key_exists("start", $slopeArray) && strlen($slopeArray["start"][0]))
            {
                $startSlope = processValue($slopeArray["start"][0]);
            }
            if (array_key_exists("end", $slopeArray) && strlen($slopeArray["end"][0]))
            {
                $endSlope = processValue($slopeArray["end"][0]);
            }
            if (array_key_exists("pattern", $slopeArray) && strlen($slopeArray["pattern"][0]))
            {
                $slopePattern = $slopeArray["pattern"];
            }
        }
    }
    if (array_key_exists("p", $_GET))
    {
        $pathString = $_GET["p"];
        if (strlen($pathString))
        {
            $pathVals = explode(",", $pathString);
            if (count($pathVals) >= 2)
            {
                $delta->x = processValue($pathVals[0]);
                $delta->y = processValue($pathVals[1]);

                if (count($pathVals) == 4)
                    $route = new Witch($delta, processValue($pathVals[2]), processValue($pathVals[3]));
                elseif (count($pathVals) == 3)
                {
                    $val = processValue($pathVals[2]);
                    $route = new Witch($delta, $val, $val);
                }
            }
        }
    }
    if (array_key_exists("n", $_GET))
    {
        $n = max(1, round(processValue($_GET["n"])));
    }

    $stroke = "none";
    $strokeWidth = 2;
    $fill = "black";

    if (array_key_exists("stroke", $_GET))
    {
        $stroke = $_GET["stroke"];
    }
    if (array_key_exists("strokeWidth", $_GET))
    {
        $strokeWidth = $_GET["strokeWidth"];
    }
    if (array_key_exists("fill", $_GET))
    {
        $fill = $_GET["fill"];
    }

    $callis = new Callis($stroke, $fill);
    $callis->strokeWidth = $strokeWidth;
    $callis->width[0] = $startWidth;

    for ($i = 0; $i < $n; ++$i)
    {
        $in = 0;
        $out = 0;

        if (count($slopePattern))
        {
            $in = -processValue($slopePattern[($i + count($slopePattern) - 1) % count($slopePattern)], 1, true);
            $out = processValue($slopePattern[$i % count($slopePattern)]);
        }
        if ($i == 0)
        {
            $in = $startSlope;
        }
        if ($i == $n - 1)
        {
            $out = $endSlope;
        }

        $w = 0;
        if (count($widthPattern))
        {
            $w = processValue($widthPattern[$i % count($widthPattern)]);
        }
        if ($i == $n - 1)
        {
            $w = $endWidth;
        }

        if (is_null($route))
            $d = vecScale($delta, 1 / $n);
        else
            $d = vecSub($route->plot(($i + 1) / $n), $callis->endPoint());

        $callis->addSegment(new Witch($d, $in, $out), $w);

    }
    for ($i = 0; $i < 4; ++$i)
    {
        $right = new Vec2($delta->mag() * 0.5, 0);
        $shift = vecRot($right, 2 * $i);
        if (is_null($path))
            $mid = vecScale($delta, 0.5);
        else
            $mid = $path->plot(0.5);
        $point = vecAdd($mid, $shift);
        $callis->fixBounds($point);
    }

    echo $callis->wrapped()."\n";

}

function parse($param)
{
    if (strspn("{}", $param) != 2)
        return NULL;

    $res = array();

    $start = explode("{", $param)[0];
    $end = explode("}", $param)[1];
    $pattern = explode("{", explode("}", $param)[0])[1];

    $startVals = explode(",", $start);
    $endVals = explode(",", $end);
    $patternVals = explode(",", $pattern);

    $res["start"] = $startVals;
    $res["end"] = $endVals;
    $res["pattern"] = $patternVals;

    return $res;
}

function processValue($val, $i = 0, $slope = false)
{
    $res = $val;

    if (strspn("|", $res))
    {
        $vals = explode("|", $res);
        $i = min($i, count($vals) - 1);
        if (count($vals))
            $res = $vals[$i];
    }
    else
        $i = 0;

    if (strspn(":", $res))
    {
        $steps = 4096;
        $range = explode(":", $res);

        $res = $range[0] + ($range[1] - $range[0]) * (rand() % ($steps + 1)) / $steps;
    }

    if ($i % 2 && $slope) 
        return -$res;
    else
        return $res;
}
?>
