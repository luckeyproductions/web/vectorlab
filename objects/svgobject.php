<?php
require 'math.php';

class SVGObject {

    public $body;
    public $o;
    public $size;

    public function __construct($x = 0, $y = 0, $w = 0, $h = 0) {
        $this->o    = new Vec2($x, $y);
        $this->size = new Vec2($w, $h);
        $this->body = "";
    }

    public function width()  { return $this->size->x; }
    public function height() { return $this->size->y; }
    public function fixBounds(Vec2 $p) {

        if ($p->x < $this->o->x) {
            $this->size->x += $this->o->x - $p->x;
            $this->o->x = $p->x;
        }
        elseif ($p->x > $this->o->x + $this->width()) {
            $this->size->x = $p->x - $this->o->x;
        }

        if ($p->y < $this->o->y) {
            $this->size->y += $this->o->y - $p->y;
            $this->o->y = $p->y;
        }
        elseif ($p->y > $this->o->y + $this->height()) {
            $this->size->y = $p->y - $this->o->y;
        }
    }

    public function body() { return $this->body; }
    public function merge(SVGObject $ob) {

        $c1 = $ob->o;
        $c2 = new Vec2($ob->o->x + $ob->width(), $ob->o->y);
        $c3 = new Vec2($ob->o->x               , $ob->o->y + $ob->height());
        $c4 = new Vec2($ob->o->x + $ob->width(), $ob->o->y + $ob->height());
        $corners = array($c1, $c2, $c3, $c4);
        foreach ($corners as $c)
            $this->fixBounds($c);

        $this->body .= $ob->body();
    }

    public function wrapped() {
        $svg = "<?xml version='1.0' encoding='UTF-8' standalone='no'?>\n\n".
                "<svg\n".
                "xmlns='http://www.w3.org/2000/svg'\n".
                "xmlns:xlink='http://www.w3.org/1999/xlink'\n".
                "width='".$this->width()."'\n".
                "height='".$this->height()."'\n".
                "viewBox='".$this->o->x." ".$this->o->y." ".$this->width()." ".$this->height()."'\n".
                "version='1.1' >\n".$this->body()."\n</svg>";

        return $svg;
    }
}

function move(Vec2 $d) { return " m ".($d->x).",".($d->y); }
function line(Vec2 $d) { return " l ".($d->x).",".($d->y); }
?>
