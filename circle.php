<? header('Content-Type: image/svg+xml');

echo '<?xml version="1.0" encoding="UTF-8" standalone="no"?>';

if (array_key_exists("r", $_GET))
    $r = $_GET["r"];
else
    $r = 4 + (rand() % 60);

$d = $r * 2;
$s = 1;

?>

<svg
   xmlns="http://www.w3.org/2000/svg"
   width="<?echo $d?>"
   height="<?echo $d?>"
   viewBox="0 0 <?echo $d?> <?echo $d?>"
   version="1.1" >
    <circle
       fill="<?echo $_GET["color"]?>"
       stroke="black"
       stroke-width="<?echo $s?>"
       id="path815"
       cx="<?echo $r?>"
       cy="<?echo $r?>"
       r="<?echo $r - $s / 2?>" />
</svg>
