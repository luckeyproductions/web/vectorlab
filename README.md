# The VectorLab

[![Oak leaf](https://vectorlab.luckey.games/objects/callis.php?w=10:20{10:200,60:100,200:40}&s=0{-1:0|-2:2,0|0:4}0:2&n=6:10&p=-200:800,-640,-1:1.5&fill=green&stroke=chartreuse&strokeWidth=1)](https://vectorlab.luckey.games/objects/callis.php?w=10:20{10:200,60:100,200:40}&s=0{-1:0|-2:2,0|0:4}0:2&n=6:10&p=-200:800,-640,-1:1.5&fill=green&stroke=chartreuse&strokeWidth=1) [![Torso](https://vectorlab.luckey.games/objects/callis.php?w=0%7B36,24,34,42,13,22%7D0&s=2%7B-0.5,-1%7C2,0.5%7C0,1,0%7C0.6,0%7D2&n=7&p=0,-192,-1:1,-0.5:0.5&fill=darksalmon&stroke=saddlebrown&strokeWidth=1)](https://vectorlab.luckey.games/objects/callis.php?w=0%7B36,24,34,42,13,22%7D0&s=2%7B-0.5,-1%7C2,0.5%7C0,1,0%7C0.6,0%7D2&n=7&p=0,-192,-1:1,-0.5:0.5&fill=darksalmon&stroke=saddlebrown&strokeWidth=1)

[![Curl](https://vectorlab.luckey.games/objects/witchcurl.php)](https://vectorlab.luckey.games/objects/witchcurl.php)
