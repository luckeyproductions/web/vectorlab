class Camera
{
    constructor()
    {
        this.pos = vec2();
        this.rot = 0;
        this.zoom = 1.;
    }

    setPosition(p)
    {
        if (p === this.pos)
            return;

        this.pos = p;
        updateCursor();
    }

    setRotation(r)
    {
        if (r === this.rot)
            return;

        while (r < 0)
            r += Math.PI * 2;

        this.rot = r % (Math.PI * 2);
        updateCursor();
    }

    setZoom(z, pivot)
    {
        z = Math.min(z, 128.);
        z = Math.max(z, .03125);

        if (z === this.zoom)
            return;

        if (arguments.length == 2)
        {
            let oldPivot = docCoords(pivot);
            this.zoom = z;
            let pivotShift = vec2.sub(docCoords(pivot), oldPivot);
            this.pos = vec2.add(this.pos, pivotShift);
        }
        else
        {
            this.zoom = z;
        }

        updateCursor();
    }
}
