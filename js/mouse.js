var mousePos = vec2();
var cursorPos = vec2();
var mouseButtons = 0;
var dragBegin = vec2();
var dragging = 0;
var dragModifiers = 0;
var hideCursor = true;

function handleMouseMove(e)
{
    var oldPos = mousePos;
    mousePos = vec2(e.pageX, e.pageY);

    updateCursor();
    hideCursor = false;

    if (dragging < 0)
    {
        startDragging();
    }
    else if (dragging & 4)
    {
        if (modifiers & 1)
        {
            cam.setZoom(cam.zoom + vec2.dot(vec2.sub(mousePos, oldPos), vec2(2, 3)) * Math.sqrt(cam.zoom) * -.001, dragBegin);
        }
        else if (modifiers & 4)
        {
            var offset = vec2(canvas.width * -.5, canvas.height * -.5);
            cam.setRotation(cam.rot + vec2.rad(vec2.add(mousePos, offset)) - vec2.rad(vec2.add(oldPos, offset)));
        }
        else
        {
            cam.setPosition(vec2.add(cam.pos, vec2.rot(vec2.div(vec2.sub(mousePos, oldPos), cam.zoom), -cam.rot)));
        }
    }

    draw();
}

function updateCursor()
{
    cursorPos = docCoords(mousePos);
}

function handleMouseWheel(e)
{
    // Change brush radius
    if (brushSize == 1)
        brushSize = 0;
    brushSize -= e.deltaY / Math.abs(e.deltaY) * (modifiers === 2 ? 32 : 4);
    if (brushSize < 1)
        brushSize = 1;

    draw();
}

function updateButtons(e)
{
    mouseButtons = e.buttons;

    if (mouseButtons === 0)
    {
        canvas.releasePointerCapture(event.pointerId);

        endDragging();
    }
    else
    {
        canvas.setPointerCapture(event.pointerId);

        if (dragging === 0)
            startDragging();
        else if (mouseButtons !== Math.abs(dragging))
            endDragging();
    }
}

function startDragging()
{
    if (dragging === 0)
    {
        dragging = -mouseButtons;
        dragBegin = mousePos;
    }
    else if (vec2.len(vec2.sub(mousePos, dragBegin)) > 2)
    {
        dragging = Math.abs(dragging);
    }

    draw();
}

function endDragging()
{
    if (dragging === 0)
        return;

    dragging = 0;

    draw();
}

function handleMouseLeaveCanvas()
{
    if (!hideCursor)
    {
        hideCursor = true;
        draw();
    }
}
