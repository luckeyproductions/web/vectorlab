var modifiers = 0;

function handleKeyDown(e)
{
    // console.log(e.key + " down");
    const rotStep = Math.PI / 12.;

    switch (e.key)
    {
    case "Control":             modifiers |= 1; break;
    case "Shift":               modifiers |= 2; break;
    case "Alt": case "Meta":    modifiers |= 4; break;
    case "+": case "=":
        if (modifiers !== 1)
        {
            cam.setZoom(cam.zoom * 2.);
            draw();
        }
    break;
    case "-":
        if (modifiers !== 1)
        {
            cam.setZoom(cam.zoom / 2.);
            draw();
        }
    break;
    case "0": case "1":
        if (modifiers !== 1)
        {
            cam.setZoom(1.);
            draw();
        }
    break;
    case "4": cam.setPosition(vec2.add(cam.pos, vec2.rot(vec2( 128 / cam.zoom, 0), -cam.rot))); draw(); break;
    case "6": cam.setPosition(vec2.add(cam.pos, vec2.rot(vec2(-128 / cam.zoom, 0), -cam.rot))); draw(); break;
    case "8": cam.setPosition(vec2.add(cam.pos, vec2.rot(vec2(0,  128 / cam.zoom), -cam.rot))); draw(); break;
    case "2": cam.setPosition(vec2.add(cam.pos, vec2.rot(vec2(0, -128 / cam.zoom), -cam.rot))); draw(); break;
    case "7": cam.setRotation(Math.round((cam.rot - rotStep * .55) / rotStep) * rotStep);       draw(); break;
    case "9": cam.setRotation(Math.round((cam.rot + rotStep * .55) / rotStep) * rotStep);       draw(); break;
    case "5": cam.setPosition(vec2()); cam.setRotation(0);                                      draw(); break;
    }
}

function handleKeyUp(e)
{
    // console.log(e.key + " up");

    if (e.key === "Control")
        modifiers &= 6;
    else if (e.key === "Shift")
        modifiers &= 5;
    else if (e.key === "Alt" || e.key === "Meta")
        modifiers &= 3;

    // console.log(modifiers);
}
