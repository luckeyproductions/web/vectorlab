function initCanvas()
{
    updateCanvasSize();

    draw();
}

function updateCanvasSize()
{
    if (!canvas)
        return;

    //let oldWidth = canvas.width;
    //let oldHeight = canvas.height;
    canvas.width  = canvas.scrollWidth;
    canvas.height = canvas.scrollHeight;

    //let sizeDelta = vec2(canvas.width - oldWidth, canvas.height - oldHeight);

    updateCursor();
    draw();
}

function draw()
{
    if (canvas.getContext)
    {
        ctx = canvas.getContext("2d");

        ctx.clearRect(0, 0, canvas.width, canvas.height);

        setViewTransform();
        drawGrid();

        ctx.translate(cam.pos.x, cam.pos.y);
        drawDocument();
        drawCursor();
        ctx.resetTransform();
    }
}

function setViewTransform()
{
    ctx.translate(canvas.width * .5, canvas.height * .5);
    ctx.rotate(cam.rot);
    ctx.scale(cam.zoom, cam.zoom);
}

function docCoords(v)
{
    return      vec2.sub(
            vec2.div(
        vec2.rot(
    vec2.sub(v, vec2(canvas.width * .5, canvas.height * .5)),
        -cam.rot),
            cam.zoom),
                cam.pos);
}

function drawGrid()
{
    let zb = Math.max(canvas.width, canvas.height) / cam.zoom;

    for (let sub = 0; sub < 2; ++sub)
    {
        let gridSize = (sub ? 32 : 128) / Math.pow(2, Math.round(Math.log2(cam.zoom)));

        ctx.beginPath();

        let x = (Math.round(((cam.pos.x + zb) % gridSize - gridSize - zb) * cam.zoom) + .5) / cam.zoom;
        while (x < canvas.width - gridSize + zb)
        {
            x += gridSize;

            ctx.moveTo(x, -zb);
            ctx.lineTo(x, canvas.height + zb);
        }

        let y = (Math.round(((cam.pos.y + zb) % gridSize - gridSize - zb) * cam.zoom) + .5) / cam.zoom;
        while (y < canvas.height - gridSize + zb)
        {
            y += gridSize;

            ctx.moveTo(-zb, y);
            ctx.lineTo(canvas.width + zb, y);
        }

        ctx.lineWidth = .5 / cam.zoom;
        ctx.strokeStyle = "#0ff1";
        ctx.stroke();
    }
}

function drawCursor()
{
    if (hideCursor)
        return;

    ctx.lineWidth = .5 / cam.zoom;
    ctx.strokeStyle = dragging > 0 ? "#f00" : "#0f0";
    ctx.beginPath();
    ctx.arc(cursorPos["x"], cursorPos["y"], brushSize, 0, 2 * Math.PI);
    ctx.stroke();
}

function drawDocument()
{
    ctx.lineWidth = 3;
    ctx.strokeStyle = "#0ff";
    ctx.beginPath();
    ctx.arc(0, 0, 128, 0, 2 * Math.PI);
    ctx.stroke();
}
